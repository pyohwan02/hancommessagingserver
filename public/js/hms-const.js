/**
 * 공통
 */
var HMS_COMMON = {
		maxSizeOfList : 100, // 목록 최대 갯수
		defaultSizeOfList : 5 // 목록 기본 갯수		
};

var HMS_STATUS = {
	ok : {code:200, name:'Ok', message:"success."},
	badRequest : {code:400, name:'BadRequest', message:"wrong parameter. Please check parameter."},
	notFound : {code:404, name:'NotFound', message:"not found unread message ID."},
	internalServerError : {code:500, name:'InternalServerError', message:"Internal Server Error"},
	unknown : {code:599, name:'Unknown', message:"unknown error"}
};

/**
 * 소켓 이벤트 종류
 */
var HMS_SOCKET_EVENT = {
		// App -> HMS -> Client
		deliveryFromApp : "deliveryFromApp", // 어플리케이션으로 부터 온 메시지.
		// HMS->Client
		msgListFromHMS : "msgListFromHMS", // 안 읽은 메시지 목록.
		readMsgAllFromHMS : "readMsgAllFromHMS", // 안 읽은 메시지 모두 읽음 처리.
		readMsgFromHMS : "readMsgFromHMS", // 안 읽은 메시지 선택하여 읽음 처리.
		cntUnreadMsgFromHMS : "cntUnreadMsgFromHMS", 
		// Client->HMS
		joinUserToHMS : "joinUserToHMS", // 사용자가 HMS에 접속.
		msgListToHMS : "msgListToHMS", // 안 읽은 메시지 목록.
		readMsgAllToHMS : "readMsgAllToHMS", // 안 읽은 메시지 모두 읽음 처리.
		readMsgToHMS : "readMsgToHMS", // 안 읽은 메시지 선택하여 읽음 처리.
		cntUnreadMsgToHMS : "cntUnreadMsgToHMS",
};

/**
 * 메시지 이벤트 종류
 */
var HMS_MSG_EVENT_TYPE = {
		createShare : "createShare", //공유 생성
		removeShare : "removeShare", //공유 해제		
};

/**
 * 메시지 읽음 종류
 */
var HMS_MSG_READ_TYPE = {
		all : 'all',
		read : 'read',
		unread : 'unread'
};