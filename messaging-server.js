var express = require('express');
var app = express();
var morgan = require('morgan');
var https = require("https");
var fs = require("fs");
var path = require("path");
var Logger = require("./logger");
var winston = new Logger();
var bodyParser = require('body-parser');
var apn = require("apn");

eval(fs.readFileSync('./public/js/hms-const.js')+'');

var sslOptions = {		
		key: fs.readFileSync('.ssl/key.pem'),
		cert: fs.readFileSync('.ssl/cert.pem')
};

var apnOptions = {
		gateway : "gateway.sandbox.push.apple.com",
		cert: './keys/cert.pem',
		key: './keys/key.pem'		
};

var port = 3000;

var server = https.createServer(sslOptions, app).listen(port, function() {
	winston.debug("Http server listening on port " + port);
});

var io = require('socket.io')(server);

if (fs.existsSync('./logs') == false) {
	fs.mkdirSync('./logs');
};

//create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(__dirname + '/logs/access.log', {flags: 'a'})

app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', "*");
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	next();
	}
);

// setup the logger
app.use(morgan('common', {stream: accessLogStream}));

app.use(function oneerror(err, req, res, next) {	
});

app.use(express.static(path.join(__dirname, "public")));

// iOS App for notification.
var apnConnection = new apn.Connection(apnOptions);

//create application/json parser
var jsonParser = bodyParser.json();	
	
//Exception Handler 등록
process.on('uncaughtException', function (err) {
	winston.debug('Caught exception: ' + err);
	//추후 trace를 하게 위해서 err.stack 을 사용하여 logging하시기 바랍니다.
	//Published story에서 beautifule logging winston 참조
});

app.get('/', function (req, res) {
	res.sendFile(__dirname + '/index.html');
});

//create connection pool for MongoDB, just do it once when sever has created.
require("./db").connect();

//binding controller
var message = require("./controller/message-controller.js");
var device = require("./controller/device-controller.js");

// 한컴큐브로부터 오는 메시지
app.post("/input/hancomcube", jsonParser, function(req, res) {

	var userIds = req.body.userIds;
	var origin = req.body.origin;
	var data = req.body.data;
	var eventType = data.eventType;

	winston.debug("input(hancomcube) data=" + JSON.stringify(data));
	//winston.debug("input(hancomcube) data.eventType=" + data.eventType);
	
	switch (data.eventType) {
	case HMS_MSG_EVENT_TYPE.createShare:
		for (var idx = 0 ; idx < userIds.length ; idx++) {

			var inpData = {userId:userIds[idx].userId, origin:origin, read:false, data:data};
			message.create(inpData, function(cbData) {
				var pushMsg = {};
				pushMsg._id = cbData._id;
				pushMsg.createDate = cbData.createDate;
				pushMsg.data = cbData.data;

				io.to(cbData.userId).emit(HMS_SOCKET_EVENT.deliveryFromApp, JSON.stringify(pushMsg));			
			});
		}
		
		break;
		
	case HMS_MSG_EVENT_TYPE.removeShare:
		
		if (userIds.length > 0) {
			for (var idx = 0 ; idx < userIds.length ; idx++) {

				var inpData = {userId:userIds[idx].userId, origin:origin, data:data};
				message.remove(inpData, function(cbData) {
					//winston.debug("remove result=" + cbData);
					
					var pushMsg = {};
					pushMsg._id = cbData._id;
					pushMsg.createDate = cbData.createDate;
					pushMsg.data = cbData.data;

					io.to(cbData.userId).emit(HMS_SOCKET_EVENT.deliveryFromApp, JSON.stringify(pushMsg));			
				});
			}
		} else {
			var inpData = {origin:origin, data:data};
			message.removeAll(inpData, function(cbData) {
				//winston.debug("remove result=" + cbData);
				
				for (var idx = 0 ; idx < cbData.length ; idx++) {
					var data = cbData[idx];
					
					var pushMsg = {};
					pushMsg._id = data._id;
					pushMsg.createDate = data.createDate;
					pushMsg.data = data.data;

					io.to(data.userId).emit(HMS_SOCKET_EVENT.deliveryFromApp, JSON.stringify(pushMsg));
				}
			});
		}		
		
		break;		
	}

	res.end();	
});

// 모바일 디바이스 토큰 저장
app.post("/device/add", jsonParser, function(req, res) {

	var userId = req.body.userId;
	var token = req.body.token;
	
	device.findOne(req.body, function(cbData) {
		
		if (cbData.isEmpty()) {
			winston.info("Add token. userId=" + userId + ", token=" + token);
			device.create(req.body, function(cbData) {
				//winston.debug("result=" + JSON.stringify(cbData));								
			});
		} else {
			winston.info("Exist token already. userId=" + userId + ", token=" + token);
		}
	});

	res.end();	
});

var nsp = io.of("/server");
nsp.on("connection", function(socket) {
	
	socket.on('sendMsg', function (data) {
		  
		  winston.debug("data=" + JSON.stringify(data));
		  
		  console.log("message", data.message);
		  console.log("scope", data.scope);
		  console.log("type", data.type);
		  
		    if (data.scope == "whole") {
		    	io.emit("msg01", JSON.stringify(data));
		    	message.create(data);
		    } else {
		    	io.to(data.scope).emit("msg01", JSON.stringify(data));
		    	message.create(data);
		    } 
		    
		    //io.to(data.roomname).emit("msg01", data.message);
		    
		  });
	  
	socket.on("getUserList", function() {	  
		  var rooms = getRoomList();		  
		  io.of("/server").emit("userList", rooms);
	});		  
	
	socket.on("notiTest", function(jsonData) {
		var inpData = JSON.parse(jsonData);

		device.list(inpData, function(cbData) {
			if (!isEmpty(cbData)) {
				for (var idx = 0 ; idx < cbData.length ; idx++) {

					var device =cbData[idx];
					winston.debug("device=" + device);
					
					var token = device.token;
					var myDevice = new apn.Device(token);

					var note = new apn.Notification();

					note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
					// 배지는 서버에서 계산해야 한다. 안 읽은 메시지 갯수를 세고 보내줘야 한다.
					//note.badge = 3;
					//note.sound = "ping.aiff";					
					//note.alert = "\uD83D\uDCE7 \u2709 You have a new message";
					note.alert = {
							"loc-key" : "SharedSampleMsg",
							"loc-args" : [ "Pyohwan", "test01.jpg"]
					};
					
					note.payload = {'messageFrom': 'Caroline'};

					apnConnection.pushNotification(note, myDevice);					
					

				}
			}
		});
	});		
});

io.on('connection', function (socket) {
	
	// 메시징 서버에 사용자 등록.
	socket.on(HMS_SOCKET_EVENT.joinUserToHMS, function (jsonData) {
	  
	  var inpData = JSON.parse(jsonData);
	  socket.join(inpData.userId);
	  winston.info("joinUserToHMS=" + inpData.userId);
  
	  /*
	  message.list(inpData, function(cbData) {
		  
		  if (!isEmpty(cbData)) {
			  socket.emit(HMS_SOCKET_EVENT.msgListFromHMS, JSON.stringify(cbData));  
		  }
	  });
	  */
  
	  var rooms = getRoomList();
	  io.of("/server").emit("userList", rooms);
	});
	
	socket.on(HMS_SOCKET_EVENT.msgListToHMS, function (jsonData) {
		winston.debug("msgListToHMS jsonData=" + jsonData);
		var inpData = JSON.parse(jsonData);
		
		message.list(inpData, function(cbData) {
			//winston.debug("messageList data=" + cbData);
			socket.emit(HMS_SOCKET_EVENT.msgListFromHMS, JSON.stringify(cbData));
		});
	});
	
	socket.on(HMS_SOCKET_EVENT.readMsgAllToHMS, function (jsonData) {
		winston.debug("readMsgAllToHMS jsonData=" + jsonData);
		var inpData = JSON.parse(jsonData);
		
		message.updateMsgAll(inpData, function(result) {

			if (result.code == HMS_STATUS.ok.code) {
				// 메시지를 읽음 처리 하면, 해당 계정이 접속중인 브라우저에 안 읽은 메시지 갯수를 알려준다.
				message.countUnreadMsg(inpData, function(count) {
					var pushMsg = {};
					pushMsg.count = count;
					
					io.to(inpData.userId).emit(HMS_SOCKET_EVENT.cntUnreadMsgFromHMS, JSON.stringify(pushMsg));
				});		
			} else {}
			
			socket.emit(HMS_SOCKET_EVENT.readMsgAllFromHMS, JSON.stringify(result));
			
		});
	});
	
	socket.on(HMS_SOCKET_EVENT.readMsgToHMS, function (jsonData) {
		winston.debug("readMsgToHMS jsonData=" + jsonData);
		var inpData = JSON.parse(jsonData);
		
		message.updateMsg(inpData, function(result) {			
			if (result.code == HMS_STATUS.ok.code) {
				// 메시지를 읽음 처리 하면, 해당 계정이 접속중인 브라우저에 안 읽은 메시지 갯수를 알려준다.
				message.countUnreadMsg(inpData, function(count) {
					var pushMsg = {};
					pushMsg.count = count;
					
					io.to(inpData.userId).emit(HMS_SOCKET_EVENT.cntUnreadMsgFromHMS, JSON.stringify(pushMsg));
				});		
			} else {}
			
			socket.emit(HMS_SOCKET_EVENT.readMsgAllFromHMS, JSON.stringify(result));
		});
	});

	/* 메시지를 1개 읽음 처리 할때.
	socket.on(HMS_SOCKET_EVENT.readMsgToHMS, function (jsonData) {
		winston.debug("jsonData=" + jsonData);
		var inpData = JSON.parse(jsonData);
		
		message.findOneAndUpdate(inpData, function(cbData) {
			
			if (isEmpty(cbData)) {
				winston.debug("this message was read already. msgId=" + inpData.messageId);
			} else {
				var pushMsg = {};
				
				pushMsg._id = cbData._id;
				pushMsg.createDate = cbData.createDate;
				pushMsg.data = cbData.data;
				
				io.to(inpData.userId).emit(HMS_SOCKET_EVENT.readMsgFromHMS, JSON.stringify(pushMsg));
			}
		});	
	});
	*/	
	
	socket.on(HMS_SOCKET_EVENT.cntUnreadMsgToHMS, function (jsonData) {
		winston.debug("jsonData=" + jsonData);
		var inpData = JSON.parse(jsonData);
		
		message.countUnreadMsg(inpData, function(count) {
			var pushMsg = {};
			pushMsg.count = count;
			
			//winston.debug("count unread msgs=" + count);
			
			socket.emit(HMS_SOCKET_EVENT.cntUnreadMsgFromHMS, JSON.stringify(pushMsg));
		});		
	});
	
});


function getRoomList() {
    var rooms = [];
    
    for (var room in io.sockets.adapter.rooms) {
    	rooms.push({"roomname":room, "userList":getAllRoomMembers(room, "/")});
    }
    
    return rooms;
}

/* 'room' (string) is required
 * '_nsp' (string) is optional, defaults to '/'
 * returns an array of Socket IDs (string) */

function getAllRoomMembers(room, _nsp) {
    var roomMembers = [];
    var nsp = (typeof _nsp !== 'string') ? '/' : _nsp;

    for( var member in io.nsps[nsp].adapter.rooms[room] ) {
        roomMembers.push(member);
    }

    return roomMembers;
}

//Speed up calls to hasOwnProperty
var hasOwnProperty = Object.prototype.hasOwnProperty;

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}