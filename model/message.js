// binding modules
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
// declare message schema
var MessageSchema = new Schema({
	userId: String,
	origin: String,
	read: Boolean,
	createDate: { type: Number, default: Date.now },
	data: mongoose.Schema.Types.Mixed
});

module.exports = mongoose.model('Message', MessageSchema);
