// binding modules
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
// declare device schema
var DeviceSchema = new Schema({
	userId: String,
	token: String,
	type: String,
	app: String
});

module.exports = mongoose.model('Device', DeviceSchema);
