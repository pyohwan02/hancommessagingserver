// binding task model
var Message = require('../model/message.js');
var Logger = require("../logger");
var fs = require("fs");
var winston = new Logger();

eval(fs.readFileSync('./public/js/hms-const.js')+'');

exports.list_old = function(req, res) {
	
	winston.debug("inputHancomcube req=" + req.body);
	winston.debug("inputHancomcube req=" + res);
	console.log(req);
	
	Message.find(function(err, msgs) {
		//console.log('Succeed to get all messages  {' + msgs + '}');	
		res.send(msgs);
		

		/*
		var todoTasks = [];
		var inProgressTasks = [];
		var doneTasks = [];

		for ( var key in msgs) {
			var task = msgs[key];
			if (task.get('status') === 'TO-DO') {
				todoTasks.push(task.get('contents'));
			} else if (task.get('status') === 'In-Progress') {
				inProgressTasks.push(task.get('contents'));
			} else if (task.get('status') === 'Done') {
				doneTasks.push(task.get('contents'));
			} else {
				console.error('Task status is not valid.' + task);
			}
		}

		res.render('index', {
			title : 'My Kanban Board',
			todoTasks : todoTasks,
			inProgressTasks : inProgressTasks,
			doneTasks : doneTasks
		});
		*/
	});
};

exports.list = function(queryData, cb) {
	
	var size = HMS_COMMON.defaultSizeOfList;
	var read = null;
	
	if (queryData.size != null && !isNaN(queryData.size)) {
		if (queryData.size > HMS_COMMON.maxSizeOfList) {
			size = HMS_COMMON.maxSizeOfList;
		} else {
			size = queryData.size;
		}
	}
	
	if (queryData.read != null) {
		if (queryData.read == HMS_MSG_READ_TYPE.read) {
			read = true;
		} else if (queryData.read == HMS_MSG_READ_TYPE.unread) {
			read = false;
		}
	}
	
	var findQuery = {};
	findQuery.userId = queryData.userId;
	
	if (read != null) {
		findQuery.read = read;
	}
	
	var query = Message.find(findQuery);
	
	if (queryData.startMsgId != null) {
		query.where('_id').gt(queryData.startMsgId);
	}
	
	query.limit(size);
	query.select("createDate read data");
	query.sort("-_id");
	query.exec(function (err, msgs) {
		if (err) throw err;
		cb(msgs);		
	});	

};

exports.findOneAndUpdate = function(queryData, cb) {
	var query1 = {_id:queryData.messageId, userId:queryData.userId, read:false};
	var query2 = {$set: {read:true}};
	var options = {upsert:false};
	
	//winston.debug("updateOne queryData=" + JSON.stringify(queryData));
	
	Message.findOneAndUpdate(query1, query2, options, function(err, msg) {
		if (err) throw err;
		cb(msg);
	});
};

exports.updateMsg = function(queryData, cb) {
	var query1 = {userId:queryData.userId, _id:{$in:queryData.msgIds}, read:false};
	var query2 = {$set: {read:true}};
	var options = {upsert:false, multi:true};
	
	//winston.debug("update queryData=" + JSON.stringify(queryData));
	
	Message.update(query1, query2, options, function(err, numberAffected, raw) {
		var result = {};
		//if (err) throw err;
		
		if (err) {
			winston.error(err);
			
			result = HMS_STATUS.internalServerError;
			result.name = err.name;
			result.message = err.message;
		} else {
			
			if (numberAffected.nModified > 0) {
				result = HMS_STATUS.ok;
			} else if (numberAffected.nModified == 0) {
				result = HMS_STATUS.notFound;
			} else {
				result = HMS_STATUS.unknown;
			}
		}
		
		//winston.debug("The number of updated documents was %d", numberAffected);		
		//winston.debug("The raw response from Mongo was", raw);
		
		cb(result);
	});
};

exports.updateMsgAll = function(queryData, cb) {
	var query1 = {userId:queryData.userId, read:false};
	var query2 = {$set: {read:true}};
	var options = {upsert:false, multi:true};
	
	//winston.debug("update queryData=" + JSON.stringify(queryData));
	
	Message.update(query1, query2, options, function(err, numberAffected, raw) {
		var result = {};
		//if (err) throw err;
		
		if (err) {
			winston.error(err);
			
			result = HMS_STATUS.internalServerError;
			result.name = err.name;
			result.message = err.message;
		} else {
			
			if (numberAffected.nModified > 0) {
				result = HMS_STATUS.ok;
			} else if (numberAffected.nModified == 0) {
				result = HMS_STATUS.notFound;
			} else {
				result = HMS_STATUS.unknown;
			}
		}
		
		//winston.debug("The number of updated documents was %d", numberAffected);		
		//winston.debug("The raw response from Mongo was", raw);
		
		cb(result);
	});
};

exports.create = function(queryData, cb) {
	//var tempData = queryData.data;
	//tempData.createDate = new Date();
	//winston.debug("Message saved. tempData=" + JSON.stringify(tempData));
	
	var message = new Message({userId:queryData.userId, origin:queryData.origin, read:queryData.read, data:queryData.data});
	message.save(function(err, result) {
		if (err) next(err);
		cb(result);
		//winston.debug("Message saved. contents=" + data.message);
	});
};

exports.remove = function(queryData, cb) {
	var findQuery = {'userId':queryData.userId, 'origin':queryData.origin, 'data.iteminfo.pathID':queryData.data.iteminfo.pathID};
	var query = Message.find(findQuery);
	query.limit(1);
	query.sort("-_id");
	query.exec(function (err, msgs) {
		if (err) {
			winston.error(err);	
		} else {
			if (msgs.length > 0) {
				Message.remove(findQuery, function(err, result) {
					if (err) {
						winston.error(err);
					} else {
						var msg = msgs[0];
						msg.data.eventType = HMS_MSG_EVENT_TYPE.removeShare;
						//winston.debug("result=" + result);
						cb(msg);
					}
				});
			} else {
				winston.debug("this message is not exist. findData=" + JSON.stringify(findQuery));
			}
		}	
	});	
};

exports.removeAll = function(queryData, cb) {
	if (isEmpty(queryData.origin) || isEmpty(queryData.data.iteminfo.pathID)) {
		winston.debug("removeAll parameter is null.");
		cb([]);
	} else {
		var findQuery = {'origin':queryData.origin, 'data.iteminfo.pathID':queryData.data.iteminfo.pathID};
		var query = Message.find(findQuery);
		query.sort("-userId");
		query.exec(function (err, msgs) {
			if (err) {
				winston.error(err);	
			} else {
				if (msgs.length > 0) {
					Message.remove(findQuery, function(err, result) {
						if (err) {
							winston.error(err);
						} else {
							var distinctMsgs = [];
							
							// 공유 해제 메시지가 중복 제거한다. insert sort와 유사. 더 좋은 방법이 없을까?
							for (var idx = 0 ; idx < msgs.length ; idx++) {
								var msg = msgs[idx];
								msg.data.eventType = HMS_MSG_EVENT_TYPE.removeShare;
								
								if (distinctMsgs.length > 0) {
									var isUnique = true;
									
									for (var distinctIdx = 0 ; distinctIdx < distinctMsgs.length ; distinctIdx++) {
										var distinctMsg = distinctMsgs[distinctIdx];
										
										if (msg.userId == distinctMsg.userId && msg.origin == distinctMsg.origin 
												&& msg.data.iteminfo.pathID == distinctMsg.data.iteminfo.pathID) {
											isUnique = false;
											break;
										}
									}
									
									if (isUnique == true) {
										distinctMsgs.push(msg);	
									}
								} else {
									distinctMsgs.push(msg);
								}
							}
							cb(distinctMsgs);
						}
					});
				} else {
					winston.debug("this message is not exist. findData=" + JSON.stringify(findQuery));
				}
			}
		});			
	}
};

exports.countUnreadMsg = function(queryData, cb) {
	var query1 = {userId:queryData.userId, read:false};
	Message.count(query1, function(err, count) {
		if (err) throw err;
		cb(count);
	});
};

//Speed up calls to hasOwnProperty
var hasOwnProperty = Object.prototype.hasOwnProperty;

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}