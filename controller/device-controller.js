// binding task model
var Device = require('../model/device.js');
var Logger = require("../logger");
var fs = require("fs");
var winston = new Logger();

eval(fs.readFileSync('./public/js/hms-const.js')+'');

exports.list = function(queryData, cb) {
	
	var query = Device.find({userId:queryData.userId});
	query.sort("-_id");
	query.exec(function (err, msgs) {
		if (err) throw err;
		cb(msgs);		
	});	

};

exports.findOneAndUpdate = function(queryData, cb) {
	var query1 = {_id:queryData.messageId, userId:queryData.userId, read:false};
	var query2 = {$set: {read:true}};
	var options = {upsert:false};
	
	//winston.debug("updateOne queryData=" + JSON.stringify(queryData));
	
	Message.findOneAndUpdate(query1, query2, options, function(err, msg) {
		if (err) throw err;
		cb(msg);
	});
};

exports.update = function(queryData, cb) {
	var query1 = {userId:queryData.userId, read:false};
	var query2 = {$set: {read:true}};
	var options = {upsert:false, multi:true};
	
	//winston.debug("update queryData=" + JSON.stringify(queryData));
	
	Message.update(query1, query2, options, function(err, numberAffected, raw) {
		if (err) throw err;
		//winston.debug("The number of updated documents was %d", numberAffected);
		//winston.debug("The raw response from Mongo was", raw);
		cb();
	});
};

exports.create = function(queryData, cb) {
	//var tempData = queryData.data;
	//tempData.createDate = new Date();
	//winston.debug("Message saved. tempData=" + JSON.stringify(tempData));
	
	var device = new Device({userId:queryData.userId, token:queryData.token, type:queryData.type, app:queryData.app});

	device.save(function(err, result) {
		if (err) next(err);
		cb(result);
		//winston.debug("Message saved. contents=" + data.message);
	});
};

exports.findOne = function(queryData, cb) {
	var query1 = {userId:queryData.userId, token:queryData.token};
	
	//winston.debug("updateOne queryData=" + JSON.stringify(queryData));
	
	Device.findOne(query1, function(err, msg) {
		if (err) throw err;
		cb(msg);
	});
};