// binding mongoose module
var mongoose = require("mongoose");
var Logger = require("./logger");
var winston = new Logger();

// connection uri
var dbURI = "mongodb://localhost/hms_default";

//exports connect function to app.js
exports.connect = function() {
	// get thedatabase connection pool
	mongoose.connect(dbURI);
	
	// DB Connection Events
	// Succeed to connect database
	mongoose.connection.on("connected", function() {
		winston.info("Succeed to get connection pool in mongoose, dbURI is " + dbURI);
	});
	
	// Failed to connect dabtabase
	mongoose.connection.on("error", function(err) {
		winston.error("Failed to get connection in mongoose, err is " + err);
	});
	
	// When the connection has disconnected
	mongoose.connection.on("disconnected", function() {
		winston.info("Database connection has disconnected.");
	});
	
	// If the Node.js process is going down, colse database
	// connection pool
	process.on("SIGINT", function() {
		mongoose.Connection.close(function() {
			winston.info("Application process is going down, disconnect database connection...");
			process.exit(0);
		});
	});
};