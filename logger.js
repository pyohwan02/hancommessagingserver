var winston = require('winston');
var filename = "logs/default-";
  
module.exports = function(){
  
var logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      level : 'debug' // Winston console log level
    }),
    new winston.transports.DailyRotateFile({
      level : 'debug',
      json : false, // json 형식으로 로깅을 하지 않는다 (단순 text)
      filename: filename,
      datePattern: 'yyyy-MM-dd.log'
    })
  ]
});
  
return logger;
};
